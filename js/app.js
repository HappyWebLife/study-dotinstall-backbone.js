(function(){

	/*----------------------------------------*/
	/* Model */
	/*----------------------------------------*/
	var Task = Backbone.Model.extend({
		defaults:{
			title: 'do something',
			completed: false
		},
		//ヴァリデーションを設定する
		validate: function(attrs) {
			if(_.isEmpty(attrs.title)){
				return "title must not be empty";
			}
		},
		//初期化
		initialize: function(){
			//invalidというイベントを関しする。
			this.on('invalid',function(model,error){
				$('#error').html(error);
			});
		}
	});

	/*----------------------------------------*/
	/* View */
	/*----------------------------------------*/
	var TaskView = Backbone.View.extend({
		tagName: 'li',
		//初期化
		initialize: function(){
			//destoryイベントを監視して、removeメソッドを実行する設定を行う。
			this.model.on('destroy',this.remove,this);
			//changeイベントを監視して、renderメソッドを実行する。
			this.model.on('change',this.render,this);
		},
		//イベント登録
		events:{
			//.deleteクラスにclickイベント:destroyを設定
			'click .delete': 'destroy',
			//.toggleクラスにclickイベント:toggleを設定
			'click .toggle': 'toggle'
		},
		//destroyメソッド設定
		destroy:function(){
			if(confirm('Are your sure?')){
				this.model.destroy();
			}
		},
		//toggleメソッド設定
		toggle: function(){
			//自身のモデルデータcompletedに、設定値と反対の値をセットする。
			this.model.set('completed',!this.model.get('completed'));
		},
		//removeメソッド設定
		remove:function(){
			this.$el.remove();
		},
		//テンプレート設定
		template: _.template($('#task-template').html()),
		//render設定
		render:function(){
			//テンプレートにモデルを突っ込んでHTML化
			var template = this.template(this.model.toJSON());
			//自身にHTMLを追加する。
			this.$el.html(template);
			return this;
		}
	});

	var TasksView = Backbone.View.extend({
		tagName: 'ul',
		initialize: function(){
			//コレクショに変更があったら、画面をサイドレンダリングしたいので、
			//addイベントを監視して、addNewメソッドを実行する。
			this.collection.on('add',this.addNew,this);
		},
		//addには、taskが引数として与えられている
		addNew: function(task){
			var taskView = new TaskView({model: task});
			this.$el.append(taskView.render().el);
		},
		//未処理のタスクを数える
		updateCound: function(){
			//collection の completed : falseだけを配列で返す。
			var uncompletedTasks = this.collection.filter(function(task){
				return !task.get('completed');
			});
			//カウントを書き込む
			$('#count').html(uncompletedTasks.length);
		},
		//render設定
		render: function(){
			//自身に設定しているコレクションに関して、処理を行う。
			//ココでは、tasksViewインスタンスを生成するときに、collection:tasksを設定している。
			this.collection.each(function(task){
				//taskの中身はtasksコレクションの配列
				//つまり、{title,completed}
				//TaskViewのインスタンスtaskViewを、modelにtaskを指定して生成する。
				var taskView = new TaskView({model:task});
				//自身に、HTMLをappend。
				//この場合は、TaskViewのrender()で生成された<li></li>に該当するもの
				this.$el.append(taskView.render().el);
			},this);
			//残りのタスク数を表示するメソッドを実行
			this.updateCound();
			//renderやったらthisを返すのがベストプラクティス見たい。
			return this;
		}
	});


	//タスクを追加するためのViewクラスを作成する。
	var AddTaskView = Backbone.View.extend({
		//すでに存在しているHTMLに紐付けする
		el: '#addTask',
		//イベントを設定
		events:{
			//submitイベントにsubmitメソッドを設定
			'submit': 'submit'
		},
		//submitメソッド
		submit: function(e){
			//バブリングキャンセル
			e.preventDefault();
			//Taskクラスのインスタンスを新しく生成
			var task  = new Task();
			//titleはフォームの#titleから取ってきて、taskにセットし、validateを行う。
			if(task.set({title: $('#title').val()},{validate: true})){
				//自身に設定しているコレクションに追加
				//addメソッドには、taskを引数として与える
				this.collection.add(task);
			}
		}
	})


	/*----------------------------------------*/
	/* Collection */
	/*----------------------------------------*/
	//Taskモデルを集めたコレクションクラスとそのインスタンスを生成
	var Tasks = Backbone.Collection.extend({model:Task});
	var tasks = new Tasks([
		{
			title: 'taks1',
			completed: true
		},
		{
			title: 'task2'
		},
		{
			title: 'task3'
		},
	]);

	//TasksViewクラスのインスタンスを、collectionにtasksコレクションを指定して生成。
	var tasksView = new TasksView({collection:tasks});
	var addTaskView = new AddTaskView({collection: tasks});

	//実行
	$('#tasks').html(tasksView.render().el);


})();
